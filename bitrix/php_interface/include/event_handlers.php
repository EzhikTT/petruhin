<?
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", Array("CIBLockHandler", "OnBeforeIBlockElementDeleteHandler"));
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("CIBLockHandler", "OnBeforeIBlockElementUpdateHandler"));

class CIBLockHandler
{
    // ������� ���������� ������� "OnBeforeIBlockElementDelete"
    function OnBeforeIBlockElementDeleteHandler($ID)
    {
        global $DB;
        global $APPLICATION;

        $delItem = CIBlockElement::GetByID($ID);
        $blockID = $delItem->Fetch()["IBLOCK_ID"];

        if ($blockID == IBLOCK_CAT_ID) {

            $delItem = CIBlockElement::GetByID($ID);
            $test = $delItem->Fetch()["SHOW_COUNTER"];

            if ($test > 1) {

                $DB->Rollback();

                $el = new CIBlockElement;
                $res = $el->Update($ID, array("ACTIVE" => "N"));

                $APPLICATION->throwException("����� ��� ���������� " . $test . " ���");

                return  false;
            }
        }
    }

    // ������� ���������� ������� "OnBeforeIBlockElementUpdate"
    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        if($arFields['IBLOCK_ID'] == IBLOCK_NEWS_ID){

            if($arFields["ACTIVE"] == "N"){

                if($arFields["ACTIVE_FROM"] >= ConvertTimeStamp(time()-86400*3)){

                    $arFields["ACTIVE"] = "Y";

                    echo "<div class=\"adm-info-message-wrap adm-info-message-red\">
                                <div class=\"adm-info-message\">
                                    <div class=\"adm-info-message-title\">������</div>
                                    �� �������� �������������� ������ �������
                                    <br>
                                    <div class=\"adm-info-message-icon\"></div>
                                </div>
                          </div>";

                    return;
                }
            }
        }
    }
}

AddEventHandler("main", "OnBeforeUserUpdate", Array("MyClass", "OnBeforeUserUpdateHandler"));

class MyClass
{
    // ������� ���������� ������� "OnAfterUserUpdate"
    function OnBeforeUserUpdateHandler(&$arFields)
    {
        $arGroupsBefore = array();
        $arGroupsAfter = array();

        $arGroupsBefore = CUser::GetUserGroup($arFields["ID"]);

        foreach($arFields["GROUP_ID"] as $aGroup){
            $arGroupsAfter[]=$aGroup["GROUP_ID"];
        }

        if((!in_array(CONTENT_MANAGER_ID, $arGroupsBefore)) && (in_array(CONTENT_MANAGER_ID, $arGroupsAfter))){

            $arFilter = Array(
                "GROUPS_ID" => Array(CONTENT_MANAGER_ID)
            );

            $rsUsers = CUser::GetList(($by = "personal_country"), ($order = "desc"), $arFilter);
            $arEmail = array();

            while ($arResUser = $rsUsers->GetNext()) {

                $arEmail[] = $arResUser["EMAIL"];
            }

            if (count($arEmail) > 0) {

                $arEventFields = array(
                    "TEXT" => "����� ������������ �������� � ���� ������",
                    "EMAIL" => implode(", ", $arEmail),
                );

                CEvent::Send("NEW_CONTENT_MANAGER", "s1", $arEventFields);
            }
        }
    }
}

AddEventHandler('form', 'onAfterResultAdd', 'my_onAfterResultAddUpdate');
//AddEventHandler('form', 'onAfterResultUpdate', 'my_onAfterResultAddUpdate');

function my_onAfterResultAddUpdate($WEB_FORM_ID, $RESULT_ID)
{
    // �������� ����������� ���������������� ������ �� ����� � ID=6
    if ($WEB_FORM_ID == 2)
    {
        // ������� � �������������� ���� 'user_ip' IP-����� ������������
        //CFormResult::SetField($RESULT_ID, 'user_ip', $_SERVER["REMOTE_ADDR"]);

        //$RESULT_ID = 189; // ID ����������

        // ������� ������ �� ���� ��������
        $arAnswer = CFormResult::GetDataByID(
            $RESULT_ID,
            array()
            //$arResult = array(),
            //$arAnswer2 = array()
        );

        //$el = new CIBlockElement;

        //$PROP = array();
        //$PROP[12] = "�����";  // �������� � ����� 12 ����������� �������� "�����"
        //$PROP[3] = 38;        // �������� � ����� 3 ����������� �������� 38

        $arLoadProductArray = Array(
            //"MODIFIED_BY"    => $USER->GetID(), // ������� ������� ������� �������������
            //"IBLOCK_SECTION_ID" => false,          // ������� ����� � ����� �������
            "IBLOCK_ID"      => 8,
            //"PROPERTY_VALUES"=> $PROP,
            "NAME"           => $arAnswer["NAME"][0]["USER_TEXT"],//"�������",
            "ACTIVE"         => "Y",            // �������
            //"PREVIEW_TEXT"   => "����� ��� ������ ���������",
            //"DETAIL_TEXT"    => "����� ��� ���������� ���������",
            //"DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/image.gif")
        );

        if(CModule::IncludeModule("iblock")) {

            $el = new CIBlockElement;
            $PRODUCT_ID = $el->Add($arLoadProductArray);//$el->Add($arLoadProductArray);

            $PROPERTY = array();
            $PROPERTY["AGE"] = $arAnswer["AGE"][0]["USER_TEXT"];
            $PROPERTY["WAGE"] = $arAnswer["WAGE"][0]["USER_TEXT"];

            $enum_ID = 0;

            if($arAnswer["GENDER"][0]["ANSWER_TEXT"] == "�������"){

                $enum_ID = 29;
            }
            else{

                $enum_ID = 28;
            }

            $PROPERTY["GENDER"] = Array("VALUE" => $enum_ID);
            $PROPERTY["RESPONDENT"] = $PRODUCT_ID;

            $arLoadProductArray = Array(
                //"MODIFIED_BY"    => $USER->GetID(), // ������� ������� ������� �������������
                //"IBLOCK_SECTION_ID" => false,          // ������� ����� � ����� �������
                "IBLOCK_ID"      => 9,
                "PROPERTY_VALUES"=> $PROPERTY,
                "NAME"           => $arAnswer["NAME"][0]["USER_TEXT"],//"�������",
                "ACTIVE"         => "Y",            // �������
                //"PREVIEW_TEXT"   => "����� ��� ������ ���������",
                //"DETAIL_TEXT"    => "����� ��� ���������� ���������",
                //"DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/image.gif")
            );

            $el = new CIBlockElement;
            $PRODUCT_ID = $el->Add($arLoadProductArray);//$el->Add($arLoadProductArray);

            //CIBlockElement::SetPropertyValues($PRODUCT_ID, 9, $arAnswer["GENDER"][0]["ANSWER_TEXT"], "GENDER");
        }
        //if($PRODUCT_ID = $el->Add($arLoadProductArray))
        //    echo "New ID: ".$PRODUCT_ID;
        //else
        //    echo "Error: ".$el->LAST_ERROR;
    }
}
