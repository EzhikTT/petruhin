<?
function dump($var, $die = false, $all = false)
{
    global $USER;
    if( ($USER->IsAdmin()) || ($all == true))
    {
        ?>
        <p><?var_dump($var)?></p>
        <?
    }
    if($die)
    {
        die;
    }
}
?>