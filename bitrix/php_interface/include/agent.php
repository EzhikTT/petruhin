<?

function AgentCheckReviews()
{
    if(CModule::IncludeModule("iblock"))
    {
        $arSelect = Array("ID", "NAME", "ACTIVE");

        // ����� ������� ���� � ������� �������� �����

        // ������� ������ �����
        //$site_format = CSite::GetDateFormat("SHORT");

        // ��������� ������ ����� � ������ PHP
        //$php_format = $DB->DateFormatToPHP($site_format);

        // ������� ������� ���� � ������� �������� �����
        //echo date($php_format, time());
        
        $arFilter = Array(
            "IBLOCK_ID" => IBLOCK_REVIEWS_SLIDER_ID,
            "<DATE_ACTIVE_TO" => ConvertTimeStamp(),
        );

        $rsResCat = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        $arItems = array();

        while($arItemCat = $rsResCat->GetNext())
        {
            $arItems[] = $arItemCat;
        }

        CEventLog::Add(array(
            "SEVERITY" => "SECURITY",
            "AUDIT_TYPE_ID" => "CHECK_REVIEWS",
            "MODULE_ID" => "iblock",
            "ITEM_ID" => "",
            "DESCRIPTION" => "�����, ����������� ���� ��������, - ".count($arItems),
        ));

        if(count($arItems) > 0)
        {
            $arFilter = Array(
                "GROUPS_ID" => Array(ADMIN_ID)
            );

            $rsUsers = CUser::GetList(($by="personal_country"), ($order="desc"), $arFilter);
            $arEmail = array();

            while($arResUser = $rsUsers->GetNext())
            {
                $arEmail[] = $arResUser["EMAIL"];
            }

            if(count($arEmail) > 0)
            {
                $arEventFields = array(
                    "TEXT" => "�����, ����������� ���� ��������, - ".count($arItems),
                    "EMAIL" => implode(", ", $arEmail),
                );

                CEvent::Send("CHECK_REVIEWS", SITE_ID, $arEventFields);
            }
        }
    }

    return "AgentCheckReviews();";
}

?>