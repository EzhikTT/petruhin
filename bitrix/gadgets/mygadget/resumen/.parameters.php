<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("iblock"))
    return false;

$arIBlocks = array("" => GetMessage("GD_PRODUCTS_EMPTY"));
$dbIBlock = CIBlock::GetList(
    array("SORT"=>"ASC", "NAME"=>"ASC"),
    array(
        "CHECK_PERMISSIONS" => "Y",
    )
);
while($arIBlock = $dbIBlock->GetNext())
    $arIBlocks[$arIBlock["ID"]] = "[".$arIBlock["ID"]."] ".$arIBlock["NAME"];


$arParameters = Array(
    "PARAMETERS"=> Array(
        "WEB_FORM_ID" => Array(
            "NAME" => GetMessage("GD_PRODUCTS_WEB_FORM_ID"),
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => '1',
            "REFRESH" => "Y",
        ),
    ),
    "USER_PARAMETERS"=> Array(
        /*"WEB_FORM_ID" => Array(
            "NAME" => GetMessage("GD_PRODUCTS_WEB_FORM_ID"),
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => '1',
            "REFRESH" => "Y",
        ),*/
        "DETAIL_URL_DEFAULT" => array(
            "NAME" => GetMessage("GD_PRODUCTS_DETAIL_URL_DEFAULT"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
        "DETAIL_URL_FILTER" => array(
            "NAME" => GetMessage("GD_PRODUCTS_DETAIL_URL_FILTER"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
    ),
);
?>