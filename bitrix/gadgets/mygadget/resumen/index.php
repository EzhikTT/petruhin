<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true){

    die();
}

$detailURLDefault = "";
$detailURLFilter = "";

#
# INPUT PARAMS
#
$arGadgetParams["WEB_FORM_ID"] = intval($arGadgetParams["WEB_FORM_ID"]);
if ($arGadgetParams["WEB_FORM_ID"] <= 0) {

	return false;
}

$arGadgetParams["DETAIL_URL_DEFAULT"] = intval($arGadgetParams["DETAIL_URL_DEFAULT"]);
if ($arGadgetParams["DETAIL_URL"] == "") {

    $detailURLDefault = "/bitrix/admin/form_result_list.php?PAGEN_1=1&SIZEN_1=20&lang=ru&WEB_FORM_ID=" . $arGadgetParams["WEB_FORM_ID"] . "&set_filter=Y&adm_filter_applied=0";
}

$arGadgetParams["DETAIL_URL_FILTER"] = intval($arGadgetParams["DETAIL_URL_FILTER"]);
if (empty($arGadgetParams["DETAIL_URL_FILTER"])) {

    $detailURLFilter = "/bitrix/admin/form_result_list.php?PAGEN_1=1&SIZEN_1=20&lang=ru&WEB_FORM_ID=" . $arGadgetParams["WEB_FORM_ID"] . "&set_filter=Y&adm_filter_applied=0&find_date_create_1_FILTER_PERIOD=day&find_date_create_1_FILTER_DIRECTION=current&find_date_create_1=" . ConvertTimeStamp() . "&find_date_create_2=" . ConvertTimeStamp();
}

$arNavParams = array(
	"nPageSize" => $arGadgetParams["ELEMENT_COUNT"],
);


if(!CModule::IncludeModule("form"))
{
	ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
	return;
}

// ������ �� ����� ����������
$arFilter = array(
    "DATE_CREATE_1"        => ConvertTimeStamp(),      // ������ "�"
    "DATE_CREATE_2"        => ConvertTimeStamp(),      // ������ "��"
);

// ������� ����������
$rsResults = CFormResult::GetList($arGadgetParams["WEB_FORM_ID"],
    ($by="s_id"),
    ($order="asc"),
    $arFilter,
    $is_filtered,
    "N");

$amoutFreshResume = 0;

while ($arResult = $rsResults->Fetch())
{
    //dump($arResult);
    $amoutFreshResume++;
}

// ������ �� ����� ����������
$arFilter = array();

// ������� ����������
$rsResults = CFormResult::GetList($arGadgetParams["WEB_FORM_ID"],
    ($by="s_id"),
    ($order="asc"),
    $arFilter,
    $is_filtered,
    "N");

$amoutResume = 0;

while ($arResult = $rsResults->Fetch()){

    $amoutResume++;
}
?>
    <p> ������ �� ������� - <a href="<?=$detailURLFilter?>"><?=$amoutFreshResume?></a></p>
    <p> ������ - <a href="<?=$detailURLDefault?>"><?=$amoutResume?></a></p>
