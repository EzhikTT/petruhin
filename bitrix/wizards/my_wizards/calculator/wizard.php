<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class ActionSelected extends CWizardStep{

    // �������������
    function InitStep(){
        // ID ����
        $this->SetStepID("action_selected");

        // ���������
        $this->SetTitle(GetMessage("AS_INIT_TITLE"));
        $this->SetSubTitle(GetMessage("AS_INIT_SUBTITLE"));

        // ���������
        $this->SetNextStep("input_number_one");
    }

    // ����� �����
    function ShowStep() {

        $this->content .= "<table class='wizard-data-table'>" . GetMessage('AS_ACTION');

        $this->content .= "<tr><th align='left'><input name=\"action\" type=\"radio\" value=\"add\" checked> ��������</td></tr>";
        $this->content .= "<tr><th align='left'><input name=\"action\" type=\"radio\" value=\"sub\"> ���������</td></tr>";
        $this->content .= "<tr><th align='left'><input name=\"action\" type=\"radio\" value=\"mul\"> ���������</td></tr>";
        $this->content .= "<tr><th align='left'><input name=\"action\" type=\"radio\" value=\"div\"> �������</td></tr>";

        $this->content .= "</table>";
    }

    // ���������� ����� �����
    function OnPostForm(){

        if(!CModule::IncludeModule('iblock')) {

            return;
        }

        $wizard =& $this->GetWizard();

        $wizard->ACTION_TYPE = $_POST['action'];

        $wizard->SetVar("ACTION_TYPE", $_POST['action']);
    }

}

class InputNumberOne extends CWizardStep{

    // �������������
    function InitStep(){
        // ID ����
        $this->SetStepID("input_number_one");

        // ���������
        $this->SetTitle(GetMessage("VAR_INIT_TITLE"));
        $this->SetSubTitle(GetMessage("INO_INIT_SUBTITLE"));

        // ���������
        $this->SetPrevStep("action_selected");
        $this->SetNextStep("input_number_two");
    }

    // ����� �����
    function ShowStep(){

        $this->content .= "<table class='wizard-data-table'>";

        // Variable A
        $this->content .= "<tr><th align='right'>" . GetMessage('AS_VAR_A') . "<span class='wizard-required'>*</span>" . "</th><td>" . $this->ShowInputField("text", "VAR_A", Array("size" => 40)) . "</td></tr>";

        $this->content .= "</table>";
        $this->content .= "<br/><div class='wizard-note-box'><span class='wizard-required'>*</span>" . GetMessage('REQ_FIELDS') . "</div>";

        Tools::validateVariable("VAR_A");
    }

    // ���������� ����� �����
    function OnPostForm(){

        if(!CModule::IncludeModule('iblock')) {

            return;
        }

        $wizard =& $this->GetWizard();

        $wizard->VARIABLE_A = $_POST[$wizard->GetRealName("VAR_A")];

        $wizard->SetVar("VARIABLE_A", $_POST[$wizard->GetRealName("VAR_A")]);
    }

}

class InputNumberTwo extends CWizardStep{

    // �������������
    function InitStep(){
        // ID ����
        $this->SetStepID("input_number_two");

        // ���������
        $this->SetTitle(GetMessage("VAR_INIT_TITLE"));
        $this->SetSubTitle(GetMessage("INT_INIT_SUBTITLE"));

        // ���������
        $this->SetPrevStep("input_number_one");
        $this->SetNextStep("validation");
    }

    // ����� �����
    function ShowStep(){

        $this->content .= "<table class='wizard-data-table'>";

        // Variable A
        $this->content .= "<tr><th align='right'>" . GetMessage('AS_VAR_B') . "<span class='wizard-required'>*</span>" . "</th><td>" . $this->ShowInputField("text", "VAR_B", Array("size" => 40)) . "</td></tr>";

        $this->content .= "</table>";
        $this->content .= "<br/><div class='wizard-note-box'><span class='wizard-required'>*</span>" . GetMessage('REQ_FIELDS') . "</div>";

        Tools::validateVariable("VAR_B");
    }

    // ���������� ����� �����
    function OnPostForm(){

        if(!CModule::IncludeModule('iblock')) {

            return;
        }

        $wizard =& $this->GetWizard();

        $wizard->VARIABLE_B = $_POST[$wizard->GetRealName("VAR_B")];

        $wizard->SetVar("VARIABLE_B", $_POST[$wizard->GetRealName("VAR_B")]);

    }
}

class Validation extends CWizardStep{

    // �������������
    function InitStep(){
        // ID ����
        $this->SetStepID("validation");

        // ���������
        $this->SetTitle(GetMessage("V_INIT_TITLE"));
        $this->SetSubTitle(GetMessage("V_INIT_SUBTITLE"));

        // ���������
        $this->SetPrevStep("input_number_two");
        $this->SetFinishStep("summary");
    }

    // ����� �����
    function ShowStep(){

        if(!CModule::IncludeModule('iblock')) {

            return;
        }

        $wizard =& $this->GetWizard();

        if($wizard->GetVar("ACTION_TYPE") == "div" && $wizard->GetVar("VARIABLE_B") == 0){

            $this->SetError(GetMessage("ERROR_MESSAGE"), "incorrect_number");

            $wizard->MESSAGE = GetMessage("ERROR_MESSAGE");

            $wizard->SetVar("MESSAGE", GetMessage("ERROR_MESSAGE"));
        }
        else{

            $this->content .= "<p> �������� ��������� ���������: " . $wizard->GetVar("VARIABLE_A");

            switch($wizard->GetVar("ACTION_TYPE")){
                case 'add':
                    $this->content .= " + ";
                    break;
                case 'div':
                    $this->content .= " / ";
                    break;
                case 'sub':
                    $this->content .= " - ";
                    break;
                case 'mul':
                    $this->content .= " * ";
                    break;
            }

            $this->content .= $wizard->GetVar("VARIABLE_B") . "</p>";
        }
    }
}

class Summary extends CWizardStep{

    // �������������
    function InitStep(){
        // ID ����
        $this->SetStepID("summary");

        // ���������
        $this->SetTitle(GetMessage("S_INIT_TITLE"));
        $this->SetSubTitle(GetMessage("S_INIT_SUBTITLE"));

        // ���������
        $this->SetCancelStep("summary");
        $this->SetCancelCaption(GetMessage("S_INIT_CANCEL_CAPTION"));
    }

    // ����� �����
    function ShowStep(){

        $wizard =& $this->GetWizard();

        if($wizard->GetVar("MESSAGE") == null){

            $wizard =& $this->GetWizard();

            $temp = "��������� ��������� " . $wizard->GetVar("VARIABLE_A");

            switch ($wizard->GetVar("ACTION_TYPE")) {
                case 'add':
                    $temp .= " + ";
                    break;
                case 'div':
                    $temp .= " / ";
                    break;
                case 'sub':
                    $temp .= " - ";
                    break;
                case 'mul':
                    $temp .= " * ";
                    break;
            }

            $temp .= $wizard->GetVar("VARIABLE_B") . " ����� ";

            $temp .=  Tools::action($wizard->GetVar("ACTION_TYPE"), $wizard->GetVar("VARIABLE_A"), $wizard->GetVar("VARIABLE_B"));

            $wizard->SetVar("MESSAGE", $temp);

            $this->content .= "<p>" . $wizard->GetVar("MESSAGE") . "</p>";

            /*CEventLog::Add(array(
                "SEVERITY" => "SECURITY",
                "AUDIT_TYPE_ID" => "WIZARD_CALCULATOR",
                "MODULE_ID" => "main",
                "ITEM_ID" => "CALCULATOR",
                "DESCRIPTION" => $wizard->GetVar("MESSAGE"),
            ));*/
        }
        else{

            $wizard =& $this->GetWizard();

            $this->content .= $wizard->GetVar("MESSAGE");
        }

        CEventLog::Add(array(
            "SEVERITY" => "SECURITY",
            "AUDIT_TYPE_ID" => "WIZARD_CALCULATOR",
            "MODULE_ID" => "main",
            "ITEM_ID" => "CALCULATOR",
            "DESCRIPTION" => $wizard->GetVar("MESSAGE"),
        ));
    }

}

class Tools{

    public function action($actionType, $A, $B){

        $res = 0;

        switch($actionType){
            case 'add':
                $res = Tools::actionAdd($A, $B);
                break;
            case 'sub':
                $res = Tools::actionSub($A, $B);
                break;
            case 'mul':
                $res = Tools::actionMul($A, $B);
                break;
            case 'div':
                $res = Tools::actionDiv($A, $B);
                break;
        }

        return $res;
    }

    public function actionAdd($A, $B){

        return (float)$A + (float)$B;
    }

    public function actionSub($A, $B){

        return (float)$A - (float)$B;
    }

    public function actionMul($A, $B){

        return (float)$A * (float)$B;
    }

    public function actionDiv($A, $B){

        $res = (float)$A / (float)$B;

        return $res;
    }

    public function validateVariable($varName){

        $wizard = $this->GetWizard();

        // JS
        $formName = $wizard->GetFormName();
        $nextButton = $wizard->GetNextButtonID();

        $var = $wizard->GetRealName($varName);

        $incorrectNumber = GetMessage('AS_INCORRECT_NUM');

        $this->content .= <<<JS
            <script type="text/javascript">
            
                function CheckMainFields()
                {
                    var form = document.forms["{$formName}"];
                    
                    if (!form)
                        return;
                    
                    var varName = form.elements["{$var}"].value;

                    var reNum = /[^\d.]/;

                    var error = "";
                    
                    var countDot = 0;;
                    
                    for(var i = 0; i < varName.length; i++){
                        
                        if(varName.charAt(i) == "."){
                            countDot++;
                        }
                    }
                    
                    var test = countDot > 1;
                    
                    if (varName.match(reNum) || test || varName.length == 0){
                        
                        error += "{$incorrectNumber}\\n";
                    }

                    if (error.length > 0)
                    {
                        alert(error);
                        return false;
                    }
                }

                function AttachEvent()
                {
                    var form = document.forms["{$formName}"];
                    if (!form)
                        return;

                    var nextButton = form.elements["{$nextButton}"];
                    if (!nextButton)
                        return;

                    nextButton.onclick = CheckMainFields;

                }

                if (window.addEventListener){
                    
                    window.addEventListener("load", AttachEvent, false);
                }
                else if (window.attachEvent){
                    
                    window.attachEvent("onload", AttachEvent);
                }
            </script>
JS;
    }
}

?>