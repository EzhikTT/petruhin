<?
// ��������� ��� ����������� �����:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mymodule/include.php"); // ������������� ������
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mymodule/prolog.php"); // ������ ������

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("mymodule");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$sTableID = "mymodule_testable"; // ID �������
$oSort = new CAdminSorting($sTableID, "ID", "desc"); // ������ ����������
$lAdmin = new CAdminList($sTableID, $oSort); // �������� ������ ������

function CheckFilter()
{
    global $FilterArr, $lAdmin;
    foreach ($FilterArr as $f) global $$f;

    // � ������ ������ ��������� ������.
    // � ����� ������ ����� ��������� �������� ���������� $find_���
    // � � ������ �������������� ������ ���������� �� �����������
    // ����������� $lAdmin->AddFilterError('�����_������').

    return count($lAdmin->arFilterErrors)==0; // ���� ������ ����, ������ false;
}

// ������ �������� �������
$FilterArr = Array(
    "find",
    "find_id",
    "find_name",
);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

// ���� ��� �������� ������� ���������, ���������� ���
if (CheckFilter())
{
    // �������� ������ ���������� ��� ������� CRubric::GetList() �� ������ �������� �������
    $arFilter = Array(
        "ID"           => ($find!="" && $find_type == "id"? $find:$find_id),
        "NAME"         => ($find!="" && $find_type == "name"? $find:$find_name),
    );
}

if($lAdmin->EditAction() && $POST_RIGHT == "W"){

    foreach($FIELDS as $ID=>$arFields){

        if(!$lAdmin->IsUpdated($ID))
            continue;

        $DB->StartTransaction();
        $ID = IntVal($ID);

        $cData = new CMyModule();
        if(($rsData = $cData->getByID($ID)) && ($arData = $rsData->Fetch()))
        {
            foreach($arFields as $key=>$value)
                $arData[$key]=$value;
            if(!$cData->Update($ID, $arData))
            {
                $lAdmin->AddGroupError(GetMessage("SAVE_ERROR ").$cData->LAST_ERROR, $ID);
                $DB->Rollback();
            }
        }
        else
        {
            $lAdmin->AddGroupError(GetMessage("SAVE_ERROR") . " " . GetMessage("NO_ID"), $ID);
            $DB->Rollback();
        }
        $DB->Commit();
    }
}

// ��������� ��������� � ��������� ��������
if(($arID = $lAdmin->GroupAction()) && $POST_RIGHT=="W" && check_bitrix_sessid())
{
    if($_REQUEST['action_target']=='selected')
    {
        $arID = Array();
        $rsData = CMyModule::GetList($by, $order, $arFilter);

        while($arRes = $rsData->Fetch())
            $arID[] = $arRes['ID'];
    }


    foreach($arID as $ID)
    {
        if(strlen($ID)<=0)
            continue;
        $ID = IntVal($ID);

        switch($_REQUEST['action'])
        {
            case "delete":
                @set_time_limit(0);
                $DB->StartTransaction();
                if(!CMyModule::Delete($ID))
                {
                    $DB->Rollback();
                    $lAdmin->AddGroupError(GetMessage("DELETE_ERROR"), $ID);
                }
                $DB->Commit();
                dump($lAdmin->ActionDoGroup($f_ID, "delete"));
                break;
        }
    }

    if (!$_REQUEST["mode"])
        LocalRedirect("mymodule_main_list.php?lang=".LANGUAGE_ID."&WEB_FORM_ID=".$WEB_FORM_ID."&additional=".$additional);
}

$myRes = new CMyModule();

$arRes = $myRes->getList($by, $order, $arFilter);

// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($arRes, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("NAV")));

$lAdmin->AddHeaders(array(
    array(
        "id"        => "ID",
        "content"   => GetMessage("TITLE_ID"),
        "sort"      => "id",
        "align"     => "center",
        "default"   => true,
    ),
    array(
        "id"        => "NAME",
        "content"   => GetMessage("TITLE_NAME"),
        "sort"      => "name",
        "align"     => "center",
        "default"   => true,
    ),
    array(
        "id"        => "TIMESTAMP_X",
        "content"   => GetMessage("TITLE_TIMESTAMP_X"),
        "sort"      => "timestamp_x",
        "align"     => "center",
        "default"   => true,
    ),
));

while($arRes = $rsData->NavNext(true, "f_")):

    // ������� ������. ��������� - ��������� ������ CAdminListRow
    $row =& $lAdmin->AddRow($f_ID, $arRes);

    $row->AddViewField("ID", $f_ID);

    $row->AddInputField("NAME", array("size"=>20));

    $row->AddCalendarField("TIMESTAMP_X", $f_TIMESTAMP_X, true);

    // ���������� ����������� ����
    $arActions = Array();

    // �������������� ��������
    $arActions[] = array(
        "ICON"=>"edit",
        "DEFAULT"=>true,
        "TEXT"=>GetMessage("EDIT"),
        "ACTION"=>$lAdmin->ActionRedirect("mymodule_main_create.php?ID=".$f_ID)
    );

    // �������� ��������
    if ($POST_RIGHT>="W") {

        $arActions[] = array(
            "ICON" => "delete",
            "TEXT" => GetMessage("DEL"),
            "ACTION" => "if(confirm(". GetMessage("CONFIRM_DEL") .")) " . $lAdmin->ActionDoGroup($f_ID, "delete")
        );
    }

    // ������� �����������
    $arActions[] = array("SEPARATOR"=>true);

    // ���� ��������� ������� - �����������, �������� �����.
    if(is_set($arActions[count($arActions)-1], "SEPARATOR"))
        unset($arActions[count($arActions)-1]);

    // �������� ����������� ���� � ������
    $row->AddActions($arActions);

endwhile;

// ���  �����, �� �� �� ��������
/*$lAdmin->AddFooter(
    array(
        array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()),
        array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
    )
);*/


$arActions = array();
$arActions=array(
    "delete"=>"DELETE",
);

$lAdmin->AddGroupActionTable($arActions);

// ���������� ���� �� ������ ������ - ���������� ��������
$aContext = array(
    array(
        "TEXT"=>GetMessage("POST_ADD"),
        "LINK"=>"mymodule_main_create.php?lang=".LANG,
        "TITLE"=>GetMessage("POST_ADD_TITLE"),
        "ICON"=>"btn_new",
    ),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// �������������� �����
$lAdmin->CheckListMode();

// ��������� ��������� ��������
$APPLICATION->SetTitle(GetMessage("TITLE"));
// �� ������� ��������� ���������� ������ � �����
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// �������� ������ �������
$oFilter = new CAdminFilter(
    $sTableID."_filter",
    array(
        "ID",
        "NAME",
    )
);
?>
    <form name="find_form" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
        <?$oFilter->Begin();?>
        <tr>
            <td><b><?=GetMessage("FIND")?>:</b></td>
            <td>
                <input type="text" size="25" name="find" value="<?echo htmlspecialchars($find)?>" title="<?=GetMessage("FIND_TITLE");?>">
                <?
                $arr = array(
                    "reference" => array(
                        GetMessage("FIND_ID"),
                        GetMessage("FIND_NAME"),
                    ),
                    "reference_id" => array(
                        "id",
                        "name",
                    )
                );
                echo SelectBoxFromArray("find_type", $arr, $find_type, "", "");
                ?>
            </td>
        </tr>
        <tr>
            <td><?=GetMessage("FIND_ID")?>:</td>
            <td>
                <input type="text" name="find_id" size="47" value="<?echo htmlspecialchars($find_id)?>">

            </td>
        </tr>
        <tr>
            <td><?=GetMessage("FIND_NAME")?>:</td>
            <td>
                <input type="text" name="find_name" size="47" value="<?echo htmlspecialchars($find_name)?>">

            </td>
        </tr>
        <?
        $oFilter->Buttons(array("table_id"=>$sTableID,"url"=>$APPLICATION->GetCurPage(),"form"=>"find_form"));
        $oFilter->End();
        ?>
    </form>

<?

// ������� ������� ������ ���������
$lAdmin->DisplayList();

// ���������� ��������
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>