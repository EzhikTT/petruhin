<?
// ��������� ��� ����������� �����:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mymodule/include.php"); // ������������� ������
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mymodule/prolog.php"); // ������ ������

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("mymodule");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

// ���������� ������ ��������
$aTabs = array(
    array("DIV" => "edit1", "TAB" => GetMessage("TAB_MAIN"), "ICON"=>"main_user_edit"),
   );
$tabControl = new CAdminTabControl("tabControl", $aTabs);

$ID = intval($ID);		// ������������� ������������� ������
$message = null;		// ��������� �� ������
$bVarsFromForm = false; // ���� "������ �������� � �����", ������������, ��� ��������� ������ �������� � �����, � �� �� ��.

if ($REQUEST_METHOD == "POST" && ($save != "" || $apply != "") && $POST_RIGHT == "W" && check_bitrix_sessid()){

    $rubric = new CMyModule;

    // ��������� ������ �����
    $arFields = Array(
        "ID"  => $ID,
        "NAME"    => $NAME,
        "TIMESTAMP_X"    => $TIMESTAMP_X,
    );

    // ���������� ������
    if($ID > 0){

        $res = $rubric->Update($ID, $arFields);
    }
    else{

        $ID = $rubric->Add($arFields);
        $res = ($ID > 0);
    }

    if($res)
    {
        // ���� ���������� ������ ������ - ������������ �� ����� ��������
        // (� ����� ������ �� ��������� �������� ����� �������� ������ "��������" � ��������)
        if ($apply != "")
            // ���� ���� ������ ������ "���������" - ���������� ������� �� �����.
            LocalRedirect("/bitrix/admin/mymodule_main_create.php?ID=".$ID."&mess=ok&lang=".LANG."&".$tabControl->ActiveTabParam());
        else
            // ���� ���� ������ ������ "���������" - ���������� � ������ ���������.
            LocalRedirect("/bitrix/admin/mymodule_main_list.php?lang=".LANG);
    }
    else
    {
        // ���� � �������� ���������� �������� ������ - �������� ����� ������ � ������ ��������������� ����������
        if($e = $APPLICATION->GetException())
            $message = new CAdminMessage(GetMessage("SAVE_ERROR"), $e);
        $bVarsFromForm = true;
    }
}

// �������� �� ���������
$str_NAME = "noname";
$str_TIMESTAMP_X = ConvertTimeStamp(false, "FULL");

// ������� ������
if($ID>0)
{
    $rubric = CMyModule::GetByID($ID);
    if(!$rubric->ExtractFields("str_"))
        $ID=0;
}

// ���� ������ �������� �� �����, �������������� ��
if($bVarsFromForm)
    $DB->InitTableVarsForEdit("mymodule_testable", "", "str_");

// ��������� ��������� ��������
$APPLICATION->SetTitle(GetMessage("TITLE"));

// �� ������� ��������� ���������� ������ � �����
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

// ������������ ����������������� ����
$aMenu = array(
    array(
        "TEXT"=>GetMessage("BACK"),
        "TITLE"=>GetMessage("BACK_TITLE"),
        "LINK"=>"mymodule_main_list.php?lang=".LANG,
        "ICON"=>"btn_list",
    )
);

if($ID>0)
{
    $aMenu[] = array("SEPARATOR"=>"Y");
    $aMenu[] = array(
        "TEXT"=>GetMessage("ADD"),
        "LINK"=>"mymodule_main_create.php?lang=".LANG,
        "ICON"=>"btn_new",
    );
    $aMenu[] = array(
        "TEXT"=>GetMessage("DEL"),
        "LINK"=>"javascript:if(confirm('".GetMessage("CONFIRM_DEL")."'))window.location='mymodule_main_list.php?ID=".$ID."&action=delete&lang=".LANG."&".bitrix_sessid_get()."';",
        "ICON"=>"btn_delete",
    );
}

// �������� ���������� ������ ����������������� ����
$context = new CAdminContextMenu($aMenu);

// ����� ����������������� ����
$context->Show();

// ���� ���� ��������� �� ������� ��� �� �������� ���������� - ������� ��.
if($_REQUEST["mess"] == "ok" && $ID>0)
    CAdminMessage::ShowMessage(array("MESSAGE"=>GetMessage("SAVE_ERROR"), "TYPE"=>"OK"));

if($message)
    echo $message->Show();
elseif($rubric->LAST_ERROR!="")
    CAdminMessage::ShowMessage($rubric->LAST_ERROR);
?>


<form method="POST" Action="<?echo $APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data" name="post_form">
<?// �������� �������������� ������ ?>
<?echo bitrix_sessid_post();?>
<?
// ��������� ��������� ��������
$tabControl->Begin();
?>
<?
//********************
// ������ �������� - ����� �������������� ���������� ��������
//********************
$tabControl->BeginNextTab();
?>
    <tr>
        <td width="40%" style="text-align: right; padding: 10px; font-weight: bold;"><?=GetMessage("TITLE_ID")?>:</td>
        <td width="60%"><?echo $ID;?></td>
    </tr>
    <tr>
        <td style="text-align: right; padding: 10px; font-weight: bold;"><?=GetMessage("TITLE_NAME")?>:</td>
        <td><input type="text" name="NAME" value="<?echo $str_NAME;?>" size="30" maxlength="100"></td>
    </tr>
    <tr>
        <td style="text-align: right; padding: 10px; font-weight: bold;"><?echo GetMessage("TITLE_TIMESTAMP_X"). " (".FORMAT_DATETIME."):"?></td>
        <td width="60%"><?echo CalendarDate("TIMESTAMP_X", $str_TIMESTAMP_X, "post_form", "20")?></td>
    </tr>

<?
// ���������� ����� - ����� ������ ���������� ���������
$tabControl->Buttons(
    array(
        "disabled"=>($POST_RIGHT<"W"),
        "back_url"=>"mymodule_main_list.php?lang=".LANG,

    )
);
?>
    <input type="hidden" name="lang" value="<?=LANG?>">
<?if($ID>0 && !$bCopy):?>
    <input type="hidden" name="ID" value="<?=$ID?>">
<?endif;

// ��������� ��������� ��������
$tabControl->End();


// �������������� ����������� �� ������� - ����� ������ ����� ����, � ������� �������� ������
$tabControl->ShowWarnings("post_form", $message);

// ���������� ��������
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>