<?
global $DB, $MESS, $APPLICATION;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/filter_tools.php");

$DBType = strtolower($DB->type);

CModule::AddAutoloadClasses(
    "mymodule",
    array(
        "CMyModule" => "classes/".$DBType."/mymodule_cmymodule.php",
        "CMyModuleEvent" => "classes/general/mymodule_cmymoduleevent.php",
        "CMyModuleUserType" => "classes/general/mymodule_cmymoduleusertype.php",
    )
);
?>