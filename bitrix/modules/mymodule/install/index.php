<?
IncludeModuleLangFile(__FILE__);

Class mymodule extends CModule
{
    const MODULE_ID = 'mymodule';
    var $MODULE_ID = 'mymodule';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $strError = '';

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . "/version.php");
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = GetMessage("MYMODULE_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("MYMODULE_MODULE_DESC");

        $this->PARTNER_NAME = GetMessage("MYMODULE_PARTNER_NAME");
        $this->PARTNER_URI = GetMessage("MYMODULE_PARTNER_URI");
    }

    function DoInstall()
    {
        global $APPLICATION, $DOCUMENT_ROOT;

        if ($GLOBALS['APPLICATION']->GetGroupRight('main') < 'W') {
            return;
        }

        $this->InstallDB();
        RegisterModule(self::MODULE_ID);

        RegisterModuleDependences("main", "OnPanelCreate", "mymodule", "CMyModuleEvent", "OnPanelCreateHandler");
        RegisterModuleDependences("main", "OnUserTypeBuildList", "mymodule", "CMyModuleUserType", "GetUserTypeDescription");

        $APPLICATION->IncludeAdminFile(GetMessage("COMPRESS_INSTALL_TITLE"), $DOCUMENT_ROOT . "/bitrix/modules/mymodule/install/step.php");
    }

    function DoUninstall()
    {
        global $DB, $APPLICATION, $step, $errors;

        $FORM_RIGHT = $APPLICATION->GetGroupRight("mymodule");
        if ($FORM_RIGHT >= "W") {
            $step = IntVal($step);
            if ($step < 2) {
                $APPLICATION->IncludeAdminFile(GetMessage("FORM_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mymodule/install/unstep.php");
            } elseif ($step == 2) {
                $errors = false;

                $this->UnInstallDB(array(
                    "savedata" => $_REQUEST["savedata"],
                ));

                $this->UnInstallFiles(array(
                    "savedata" => $_REQUEST["savedata"],
                ));

                $APPLICATION->IncludeAdminFile(GetMessage("FORM_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mymodule/install/unstep1.php");
            }
        }
    }

    function InstallDB($arParams = array())
    {
        global $DB, $DBType;
        $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/' . self::MODULE_ID . '/install/db/' . strtolower($DBType) . '/install.sql');

        return true;
    }

    function UnInstallDB($arParams = array())
    {

        global $APPLICATION, $DB, $errors;

        if (!array_key_exists("savedata", $arParams) || $arParams["savedata"] != "Y") {
            $errors = false;
            // delete whole base
            $errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . self::MODULE_ID . "/install/db/" . strtolower($DBType) . "/uninstall.sql");

            if (!empty($errors)) {
                $APPLICATION->ThrowException(implode("", $errors));
                return false;
            }

        }

        UnRegisterModule(self::MODULE_ID);

        UnRegisterModuleDependences("main", "OnPanelCreate", "mymodule", "CMyModuleEvent", "OnPanelCreateHandler");
        UnRegisterModuleDependences("main", "OnUserTypeBuildList", "mymodule", "CMyModuleUserType", "GetUserTypeDescription");

        return true;
    }

    function InstallFiles()
    {
        if ($_ENV["COMPUTERNAME"] != 'BX') {

            CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mymodule/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true);
            CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mymodule/install/components/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/.default/components/", true, true);
        }
        return true;
    }


    function UnInstallFiles($arParams = array())
    {
        global $DB;

        // Delete files
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/mymodule/install/admin/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
        DeleteDirFilesEx("/bitrix/templates/.default/components/bitrix/system.field.view/media_id/");

        return true;
    }

}

?>