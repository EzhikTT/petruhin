<?

class CMyModule
{
    var $LAST_ERROR = "";

    function err_mess()
    {
        $module_id = "mymodule";
        @include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/" . $module_id . "/install/version.php");
        return "<br>Module: " . $module_id . " (" . $arModuleVersion["VERSION"] . ")<br>Class: CMyModule<br>File: " . __FILE__;
    }

    function getList($by = "id", $order = "asc", $arFilter = array())
    {

        global $DB;

        if (!in_array($by, array("id", "name", "timestamp_x"))) {

            $by = "id";
        }

        if (!in_array($order, array("asc", "desc"))) {

            $order = "asc";
        }

        $sFilter = "";

        $conditions = array();

        foreach ($arFilter as $item) {

            if ($item != null) {

                if (gettype($arFilter[key($arFilter)]) == "string") {

                    array_push($conditions, key($arFilter) . "=\"" . $arFilter[key($arFilter)] . "\"");
                } else {

                    array_push($conditions, key($arFilter) . "=" . $arFilter[key($arFilter)]);
                }
            }

            next($arFilter);
        }


        if (count($conditions) > 0) {

            $sFilter = "\nWHERE " . implode("\nAND ", $conditions);
        }

        $strSql = "
            SELECT 
				ID,
				NAME,
				TIMESTAMP_X
			FROM 
				mymodule_testable" .
            $sFilter . "
			ORDER BY " . $by . " " . $order . " ";

        $arResult = array();
        $obItem = $DB->Query($strSql);

        while ($arItem = $obItem->Fetch()) {

            $arResult[] = $arItem;
        }

        return $arResult;
    }

    function getByID($ID)
    {

        global $DB;
        $ID = intval($ID);

        $strSql = "
            SELECT 
				*
			FROM 
				mymodule_testable
			WHERE 
			    ID =" . $ID . "
	   ";

        return $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
    }

    function Delete($ID)
    {
        global $DB;
        $ID = intval($ID);

        $DB->StartTransaction();

        $res = $DB->Query("DELETE FROM mymodule_testable WHERE ID='$ID'", false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

        if ($res)
            $DB->Commit();
        else
            $DB->Rollback();

        return $res;
    }

    function Add($arFields)
    {
        global $DB;

        if (!$this->CheckFields($arFields))
            return false;

        $ID = $DB->Add("mymodule_testable", $arFields);

        return $ID;
    }

    function Update($ID, $arFields)
    {
        global $DB;
        $ID = intval($ID);

        $strUpdate = $DB->PrepareUpdate("mymodule_testable", $arFields);
        if ($strUpdate != "") {
            $strSql = "UPDATE mymodule_testable SET " . $strUpdate . " WHERE ID=" . $ID;
            $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
        }
        return true;
    }
}

?>