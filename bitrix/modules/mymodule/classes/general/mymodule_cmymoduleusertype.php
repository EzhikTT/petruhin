<?

class CMyModuleUserType extends CUserTypeIBlockSection
{
    function GetUserTypeDescription()
    {
        return array(
            "USER_TYPE_ID" => "media_id",
            "CLASS_NAME" => "CMyModuleUserType",
            "DESCRIPTION" => "�������� � ��������������",
            "BASE_TYPE" => "enum",
        );
    }

    function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
    {
        $result = '';

        $result .= '
		<tr>
			<td class="adm-detail-valign-top">' . GetMessage("USER_TYPE_ENUM_DISPLAY") . ':</td>
			<td>
				<label><input type="radio" name="' . $arHtmlControl["NAME"] . '[DISPLAY]" value="LIST" ' . ("LIST" == $value ? 'checked="checked"' : '') . '>' . GetMessage("USER_TYPE_ENUM_LIST") . '</label><br>
				<label><input type="radio" name="' . $arHtmlControl["NAME"] . '[DISPLAY]" value="CHECKBOX" ' . ("CHECKBOX" == $value ? 'checked="checked"' : '') . '>' . GetMessage("USER_TYPE_ENUM_CHECKBOX") . '</label><br>
			</td>
		</tr>
		';

        if ($bVarsFromForm)
            $value = intval($GLOBALS[$arHtmlControl["NAME"]]["LIST_HEIGHT"]);
        elseif (is_array($arUserField))
            $value = intval($arUserField["SETTINGS"]["LIST_HEIGHT"]);
        else
            $value = 5;
        $result .= '
		<tr>
			<td>' . GetMessage("USER_TYPE_ENUM_LIST_HEIGHT") . ':</td>
			<td>
				<input type="text" name="' . $arHtmlControl["NAME"] . '[LIST_HEIGHT]" size="10" value="' . $value . '">
			</td>
		</tr>
		';

        return $result;
    }

    function GetAdminListViewHTML($arUserField, $arHtmlControl)
    {
        static $cache = array();
        $empty_caption = '&nbsp;';

        if (!array_key_exists($arHtmlControl["VALUE"], $cache)) {
            $rsEnum = call_user_func_array(
                array($arUserField["USER_TYPE"]["CLASS_NAME"], "getlist"),
                array(
                    $arUserField,
                )
            );
            if (!$rsEnum)
                return $empty_caption;

            foreach ($rsEnum->arResult as $arEnum) {
                $val = $arEnum["NAME"] . " (" . "ID = " . $arEnum["ID"] . ")";
                $cache[$arEnum["ID"]] = $val;
            }
        }
        if (!array_key_exists($arHtmlControl["VALUE"], $cache))
            $cache[$arHtmlControl["VALUE"]] = $empty_caption;
        return $cache[$arHtmlControl["VALUE"]];

    }

    function GetList($arUserField)
    {
        CModule::IncludeModule("fileman");
        CMedialib::Init();

        $dbRes = CMedialibCollection::GetList(array('arOrder' => Array('NAME' => 'ASC'), 'arFilter' => array('ACTIVE' => 'Y'))); //CIBlockSection::GetList(array('left_margin' => 'asc'), array('ID' => $arResult['VALUE']), false, array("ID", "NAME"));

        return new CIBlockSectionEnum($dbRes);
    }

    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        $result = '';

        $rsEnum = call_user_func_array(
            array($arUserField["USER_TYPE"]["CLASS_NAME"], "getlist"),
            array(
                $arUserField,
            )
        );

        if ($arUserField["SETTINGS"]["DISPLAY"] == "CHECKBOX") {
            $bWasSelect = false;
            $result2 = '';

            foreach ($rsEnum->arResult as $arEnum) {
                $bSelected = (
                    ($arHtmlControl["VALUE"] == $arEnum["ID"]) ||
                    ($arUserField["ENTITY_VALUE_ID"] <= 0 && $arEnum["DEF"] == "Y")
                );
                $bWasSelect = $bWasSelect || $bSelected;
                $result2 .= '<label><input type="radio" value="' . $arEnum["ID"] . '" name="' . $arHtmlControl["NAME"] . '"' . ($bSelected ? ' checked' : '') . ($arUserField["EDIT_IN_LIST"] != "Y" ? ' disabled="disabled" ' : '') . '>' . $arEnum["NAME"] . '</label><br>';
            }
            if ($arUserField["MANDATORY"] != "Y")
                $result .= '<label><input type="radio" value="" name="' . $arHtmlControl["NAME"] . '"' . (!$bWasSelect ? ' checked' : '') . ($arUserField["EDIT_IN_LIST"] != "Y" ? ' disabled="disabled" ' : '') . '>' . htmlspecialcharsbx(strlen($arUserField["SETTINGS"]["CAPTION_NO_VALUE"]) > 0 ? $arUserField["SETTINGS"]["CAPTION_NO_VALUE"] : GetMessage('MAIN_NO')) . '</label><br>';
            $result .= $result2;
        } else {
            $bWasSelect = false;
            $result2 = '';

            foreach ($rsEnum->arResult as $arEnum) {
                $bSelected = (
                    ($arHtmlControl["VALUE"] == $arEnum["ID"]) ||
                    ($arUserField["ENTITY_VALUE_ID"] <= 0 && $arEnum["DEF"] == "Y")
                );
                $bWasSelect = $bWasSelect || $bSelected;
                $result2 .= '<option value="' . $arEnum["ID"] . '"' . ($bSelected ? ' selected' : '') . '>' . $arEnum["NAME"] . '</option>';
            }

            if ($arUserField["SETTINGS"]["LIST_HEIGHT"] > 1) {
                $size = ' size="' . $arUserField["SETTINGS"]["LIST_HEIGHT"] . '"';
            } else {
                $arHtmlControl["VALIGN"] = "middle";
                $size = '';
            }

            $result = '<select name="' . $arHtmlControl["NAME"] . '"' . $size . ($arUserField["EDIT_IN_LIST"] != "Y" ? ' disabled="disabled" ' : '') . '>';
            if ($arUserField["MANDATORY"] != "Y") {
                $result .= '<option value=""' . (!$bWasSelect ? ' selected' : '') . '>' . htmlspecialcharsbx(strlen($arUserField["SETTINGS"]["CAPTION_NO_VALUE"]) > 0 ? $arUserField["SETTINGS"]["CAPTION_NO_VALUE"] : GetMessage('MAIN_NO')) . '</option>';
            }
            $result .= $result2;
            $result .= '</select>';
        }

        return $result;
    }
}