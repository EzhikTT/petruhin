<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("T_IBLOCK_DESC_VACANCY_LIST"),
	"DESCRIPTION" => GetMessage("T_IBLOCK_DESC_VACANCY_DESC"),
	"ICON" => "/images/news_list.gif",
	"SORT" => 20,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "content",
		"CHILD" => array(
			"ID" => "vacancy_ext",
			"NAME" => GetMessage("T_IBLOCK_DESC_VACANCY"),
			"SORT" => 10,
			"CHILD" => array(
				"ID" => "vacancy_cmpx",
			),
		),
	),
);

?>