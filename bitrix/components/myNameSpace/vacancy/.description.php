<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("IBLOCK_VACANCY_NAME"),
	"DESCRIPTION" => GetMessage("IBLOCK_VACANCY_DESCRIPTION"),
	"ICON" => "/images/vacancy_all.gif",
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "content",
		"CHILD" => array(
			"ID" => "vacancy",
			"NAME" => GetMessage("T_IBLOCK_DESC_VACANCY"),
			"SORT" => 10,
			"CHILD" => array(
				"ID" => "vacancy_cmpx",
			),
		),
	),
);

?>