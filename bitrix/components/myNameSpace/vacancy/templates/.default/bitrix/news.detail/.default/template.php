<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h3><?= $arResult["NAME"] ?></h3>
<ul>
    <li class="vacancy_description" style="list-style-type: none; padding: 0 15px 15px 15px">
        <? foreach ($arResult["PROPERTIES"] as $arProp): ?>

            <br/><strong><?= $arProp["NAME"] ?>:</strong>
            <br/><?= $arProp["VALUE"] ?>
        <? endforeach; ?>

        <br/><br/><a href="<?=$arResult["DETAIL_PAGE_URL"]?><?=$arResult["ID"]?>/resume/">��������� ������</a>
    </li>
</ul>

