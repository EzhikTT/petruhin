<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("ul.submenu").hide();//�������� ��� ��������� ������
		jQuery("span.expand").click(function(){
			var e = jQuery(this).next();
			e.slideToggle("fast");//�����������/�������������
		});
	});
</script>


<div class="vc_content">
	<ul>
		<?foreach($arResult["ITEMS"] as $arHead):?>
			<li><span class="expand"><h2 style="color: blue"><?=$arHead["NAME"]?> (<?=count($arHead);?>)</h2></span>
				<ul class="submenu">
					<?foreach($arHead["ITEMS"] as $arItem):?>
						<li><h3><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h3>
							<ul>
								<li class="vacancy_description">
									<?foreach($arItem["PROPERTIES"] as $arProp):?>
										<br/><strong><?=$arProp["NAME"]?>:</strong>
										<br/><?=$arProp["VALUE"]?>
									<?endforeach;?>
								</li>
							</ul>
						</li>
					<?endforeach;?>
				</ul>
			</li>
		<?endforeach;?>
	</ul>
</div>