<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("MYIBLOCKS_NAME"),
	"DESCRIPTION" => GetMessage("MYIBLOCKS_DESCRIPTION"),
	"ICON" => "/images/myiblocks.gif",
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "content",
		"CHILD" => array(
			"ID" => "myiblocks",
			"NAME" => GetMessage("T_MYIBLOCKS_DESC"),
			"SORT" => 10,
			"CHILD" => array(
				"ID" => "myiblocks_cmpx",
			),
		),
	),
);

?>