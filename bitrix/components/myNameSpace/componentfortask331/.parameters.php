<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule("iblock"))
    return;

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE" => "Y"));
while ($arr = $rsIBlock->Fetch()) {
    $arIBlock[$arr["ID"]] = "[" . $arr["ID"] . "] " . $arr["NAME"];
}

$arSorts = Array("ASC" => GetMessage("T_IBLOCK_DESC_ASC"), "DESC" => GetMessage("T_IBLOCK_DESC_DESC"));
$arSortFields = Array(
    "ID" => GetMessage("T_IBLOCK_DESC_FID"),
    "NAME" => GetMessage("T_IBLOCK_DESC_FNAME"),
    "ACTIVE_FROM" => GetMessage("T_IBLOCK_DESC_FACT"),
    "SORT" => GetMessage("T_IBLOCK_DESC_FSORT"),
    "TIMESTAMP_X" => GetMessage("T_IBLOCK_DESC_FTSAMP")
);

$arProperty_LNS = array();

foreach ($arCurrentValues['IBLOCK_ID'] as $item) {

    $rsProp = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $item));

    while ($arr = $rsProp->Fetch()) {
        $arProperty[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];

        if (in_array($arr["PROPERTY_TYPE"], array("L", "N", "S", "E")) && $arr["CODE"] != "RESPONDENT") {
            $arProperty_LNS[$arr["CODE"]] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
        }
    }
}

$arUGroupsEx = Array();
$dbUGroups = CGroup::GetList($by = "c_sort", $order = "asc");
while ($arUGroups = $dbUGroups->Fetch()) {
    $arUGroupsEx[$arUGroups["ID"]] = $arUGroups["NAME"];
}

$arComponentParameters = array(
    "PARAMETERS" => array(
        "VARIABLE_ALIASES" => Array(
            "SECTION_CODE" => Array("NAME" => "SECTION_CODE"),
        ),

        "SEF_MODE" => Array(
            "main_page" => array(
                "NAME" => "HOME",
                "DEFAULT" => "",
                "VARIABLES" => array(),
            ),
            "main_form" => array(
                "NAME" => "FORM",
                "DEFAULT" => "form/",
                "VARIABLES" => array("SECTION_CODE", "IBLOCK_ID"),
            ),
            "main_list" => array(
                "NAME" => "LIST",
                "DEFAULT" => "list/",
                "VARIABLES" => array("SECTION_CODE", "IBLOCK_ID"),
            ),
        ),
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("BN_P_IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("BN_P_IBLOCK"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlock,
            "MULTIPLE" => "Y",
        ),
        "USE_FILTER" => Array(
            "PARENT" => "FILTER_SETTINGS",
            "NAME" => GetMessage("T_IBLOCK_DESC_USE_FILTER"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
            "REFRESH" => "Y",
        ),
    ),
);

if ($arCurrentValues["USE_FILTER"] == "Y") {
    $arComponentParameters["PARAMETERS"]["FILTER_PROPERTY_CODE"] = array(
        "PARENT" => "FILTER_SETTINGS",
        "NAME" => GetMessage("T_IBLOCK_PROPERTY"),
        "TYPE" => "LIST",
        "MULTIPLE" => "Y",
        "VALUES" => $arProperty_LNS,
    );
}