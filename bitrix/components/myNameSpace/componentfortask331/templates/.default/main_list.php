<? //if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
?>

<? $APPLICATION->IncludeComponent(
    "bitrix:form.result.list",
    "resp_res",
    array(
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "NEW_URL" => "",
        "NOT_SHOW_FILTER" => array(
            0 => "SIMPLE_QUESTION_489",
            1 => "",
        ),
        "NOT_SHOW_TABLE" => array(
            0 => "SIMPLE_QUESTION_489",
            1 => "",
        ),
        "SEF_MODE" => "N",
        "SHOW_ADDITIONAL" => "N",
        "SHOW_ANSWER_VALUE" => "N",
        "SHOW_STATUS" => "N",
        "VIEW_URL" => "",
        "WEB_FORM_ID" => "2",
        "COMPONENT_TEMPLATE" => "resp_res",
        "FILTER_FIELDS" => $arParams["FILTER_PROPERTY_CODE"],
    ),
    $component,
    false
); ?><br>
