<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

?>
<script>
    <!--
    function Form_Filter_Click_<?=$arResult["filter_id"]?>()
    {
        var sName = "<?=$arResult["tf_name"]?>";
        var filter_id = "form_filter_<?=$arResult["filter_id"]?>";
        var form_handle = document.getElementById(filter_id);

        if (form_handle)
        {
            if (form_handle.className != "form-filter-none")
            {
                form_handle.className = "form-filter-none";
                document.cookie = sName+"="+"none"+"; expires=Fri, 31 Dec 2030 23:59:59 GMT;";
            }
            else
            {
                form_handle.className = "form-filter-inline";
                document.cookie = sName+"="+"inline"+"; expires=Fri, 31 Dec 2030 23:59:59 GMT;";
            }
        }
    }
    //-->
</script>
<p>
    <?=($arResult["is_filtered"] ? "<span class='form-filteron'>".GetMessage("FORM_FILTER_ON") : "<span class='form-filteroff'>".GetMessage("FORM_FILTER_OFF"))?></span>&nbsp;&nbsp;&nbsp;
    [ <a href="javascript:void(0)" OnClick="Form_Filter_Click_<?=$arResult["filter_id"]?>()"><?=GetMessage("FORM_FILTER")?></a> ]
</p>
<?
/***********************************************
filter
 ************************************************/
?>
<form name="form1" method="GET" action="<?=$APPLICATION->GetCurPageParam("", array("sessid", "delete", "del_id", "action"), false)?>?" id="form_filter_<?=$arResult["filter_id"]?>" class="form-filter-<?=htmlspecialcharsbx($arResult["tf"]);?>">
    <input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>" />
    <?if ($arParams["SEF_MODE"] == "N"):?><input type="hidden" name="action" value="list" /><?endif?>
    <table class="form-filter-table data-table">

        <tbody>
        <?
        if (strlen($arResult["str_error"]) > 0)
        {
            ?>
            <tr>
                <td class="errortext" colspan="2"><?=$arResult["str_error"]?></td>
            </tr>
            <?
        } // endif (strlen($str_error) > 0)
        ?>
        <?
        if ($arParams["SHOW_STATUS"]=="Y")
        {
            ?>
            <tr>
                <td><?=GetMessage("FORM_F_STATUS")?></td>
                <td><select name="find_status" id="find_status">
                        <option value="NOT_REF"><?=GetMessage("FORM_ALL")?></option>
                        <?
                        foreach ($arResult["arStatuses_VIEW"] as $arStatus)
                        {
                            ?>
                            <option value="<?=$arStatus["REFERENCE_ID"]?>"<?=($arStatus["REFERENCE_ID"]==$arResult["__find"]["find_status"] ? " SELECTED=\"1\"" : "")?>><?=$arStatus["REFERENCE"]?></option>
                            <?
                        }
                        ?>
                    </select></td>
            </tr>
            <tr>
                <td><?=GetMessage("FORM_F_STATUS_ID")?></td>
                <td><?echo CForm::GetTextFilter("status_id", 45, "", "");?></td>
            </tr>
            <?
        } //endif ($SHOW_STATUS=="Y");
        ?>
        <?
        if (is_array($arResult["arrFORM_FILTER"]) && count($arResult["arrFORM_FILTER"])>0)
        {
        ?>
        <?
        if ($arParams["F_RIGHT"] >= 25)
        {
            ?>
            <tr>
                <th colspan="2"><?=GetMessage("FORM_QA_FILTER_TITLE")?></th>
            </tr>
            <?
        } // endif ($F_RIGHT>=25);
        ?>

        <?

        unset($arResult["arrFORM_FILTER"]["NAME"]);

        foreach ($arResult["arrFORM_FILTER"] as $arrFILTER)
        {
        $prev_fname = "";

        foreach ($arrFILTER as $arrF)
        {

        if ($arParams["SHOW_ADDITIONAL"] == "Y" || $arrF["ADDITIONAL"] != "Y")
        {
        $i++;
        if ($arrF["SID"]!=$prev_fname)
        {
        if ($i>1)
        {
            ?>
            </td>
            </tr>
            <?
        } //endif($i>1);
        ?>
        <tr>
            <td>
                <?=$arrF["FILTER_TITLE"] ? $arrF['FILTER_TITLE'] : $arrF['TITLE']?>
                <?=($arrF["FILTER_TYPE"]=="date" ? " (".CSite::GetDateFormat("SHORT").")" : "")?>
            </td>
            <td>
                <?
                } //endif ($fname!=$prev_fname) ;
                ?>
                <?
                switch($arrF["FILTER_TYPE"]){
                    case "text":
                        echo CForm::GetNumberFilter($arrF["FID"]);
                        break;
                    case "dropdown":
                        echo CForm::GetDropDownFilter($arrF["ID"], $arrF["PARAMETER_NAME"], $arrF["FID"]);
                        break;
                } // endswitch
                ?>
                <?
                if ($arrF["PARAMETER_NAME"]=="ANSWER_TEXT")
                {
                    ?>
                    &nbsp;[<span class='form-anstext'>...</span>]
                    <?
                }
                elseif ($arrF["PARAMETER_NAME"]=="ANSWER_VALUE")
                {
                    ?>
                    &nbsp;(<span class='form-ansvalue'>...</span>)
                    <?
                }
                ?>
                <br />
                <?
                $prev_fname = $arrF["SID"];
                } //endif (($arrF["ADDITIONAL"]=="Y" && $SHOW_ADDITIONAL=="Y") || $arrF["ADDITIONAL"]!="Y");

                } // endwhile (list($key, $arrF) = each($arrFILTER));

                } // endwhile (list($key, $arrFILTER) = each($arrFORM_FILTER));
                } // endif(is_array($arrFORM_FILTER) && count($arrFORM_FILTER)>0);
                ?></td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <th colspan="2">
                <input type="submit" name="set_filter" value="<?=GetMessage("FORM_F_SET_FILTER")?>" /><input type="hidden" name="set_filter" value="Y" />&nbsp;&nbsp;<input type="submit" name="del_filter" value="<?=GetMessage("FORM_F_DEL_FILTER")?>" />
            </th>
        </tr>
        </tfoot>
    </table>
</form>
<br />

<?
if ($arParams["can_delete_some"])
{
    ?>
    <SCRIPT LANGUAGE="JavaScript">
        <!--
        function OnDelete_<?=$arResult["filter_id"]?>()
        {
            var show_conf;
            var arCheckbox = document.forms['rform_<?=$arResult["filter_id"]?>'].elements["ARR_RESULT[]"];
            if(!arCheckbox) return;
            if(arCheckbox.length>0 || arCheckbox.value>0)
            {
                show_conf = false;
                if (arCheckbox.value>0 && arCheckbox.checked) show_conf = true;
                else
                {
                    for(i=0; i<arCheckbox.length; i++)
                    {
                        if (arCheckbox[i].checked)
                        {
                            show_conf = true;
                            break;
                        }
                    }
                }
                if (show_conf)
                    return confirm("<?=GetMessage("FORM_DELETE_CONFIRMATION")?>");
                else
                    alert('<?=GetMessage("FORM_SELECT_RESULTS")?>');
            }
            return false;
        }

        function OnSelectAll_<?=$arResult["filter_id"]?>(fl)
        {
            var arCheckbox = document.forms['rform_<?=$arResult["filter_id"]?>'].elements["ARR_RESULT[]"];
            if(!arCheckbox) return;
            if(arCheckbox.length>0)
                for(i=0; i<arCheckbox.length; i++)
                    arCheckbox[i].checked = fl;
            else
                arCheckbox.checked = fl;
        }
        //-->
    </SCRIPT>
    <?
} //endif($can_delete_some);
?>

<?
if (strlen($arResult["FORM_ERROR"]) > 0) ShowError($arResult["FORM_ERROR"]);
if (strlen($arResult["FORM_NOTE"]) > 0) ShowNote($arResult["FORM_NOTE"]);
?>
<form name="rform_<?=$arResult["filter_id"]?>" method="post" action="<?=POST_FORM_ACTION_URI?>#nav_start">
    <input type="hidden" name="WEB_FORM_ID" value="<?=$arParams["WEB_FORM_ID"]?>" />
    <?=bitrix_sessid_post()?>


    <table class="form-table data-table">
        <?
        /***********************************************
        table header
         ************************************************/
        ?>
        <thead>
        <tr>
            <th><?=GetMessage("FORM_TIMESTAMP")?><br /><?=SortingEx("s_timestamp")?></th>
            <?
            if ($arParams["F_RIGHT"] >= 25)
            {
                ?>
                <th>
                    <table class="form-results-header-inline">
                        <?
                        if ($arParams["isStatisticIncluded"])
                        {
                            ?>
                            <tr>
                                <th><?=GetMessage("FORM_USER")?></th>
                                <td><?=SortingEx("s_user_id")?></td>
                            </tr>
                            <tr>
                                <th><?=GetMessage("FORM_GUEST_ID")?></th>
                                <td><?=SortingEx("s_guest_id")?></td>
                            </tr>
                            <tr>
                                <th><?=GetMessage("FORM_SESSION_ID")?></th>
                                <td><?=SortingEx("s_session_id")?></td>
                            </tr>
                            <?
                        }
                        else
                        {?>
                            <tr>
                                <td><?=GetMessage("FORM_USER")?></td>
                            </tr>
                            <tr>
                                <td><?=SortingEx("s_user_id")?></td>
                            </tr>
                            <?
                        } //endif(isStatisticIncluded);
                        ?>
                    </table>
                </th>
                <?
            } //endif;($F_RIGHT>=25)
            ?>
            <?
            $colspan = 4;
            if (is_array($arResult["arrColumns"]))
            {
                foreach ($arResult["arrColumns"] as $arrCol)
                {
                    if (!is_array($arParams["arrNOT_SHOW_TABLE"]) || !in_array($arrCol["SID"], $arParams["arrNOT_SHOW_TABLE"]))
                    {
                        if (($arrCol["ADDITIONAL"]=="Y" && $arParams["SHOW_ADDITIONAL"]=="Y") || $arrCol["ADDITIONAL"]!="Y")
                        {
                            $colspan++;
                            ?>
                            <th>
                            <?
                            if ($arParams["F_RIGHT"] >= 25)
                            {
                                ?>
                                [<a title="<?=GetMessage("FORM_FIELD_PARAMS")?>" href="/bitrix/admin/form_field_edit.php?lang=<?=LANGUAGE_ID?>&ID=<?=$arrCol["ID"]?>&FORM_ID=<?=$arParams["WEB_FORM_ID"]?>&WEB_FORM_ID=<?=$arParams["WEB_FORM_ID"]?>&additional=<?=$arrCol["ADDITIONAL"]?>"><?=$arrCol["ID"]?></a>]<br /><?
                            }//endif($F_RIGHT>=25);
                            ?>
                            <?=$arrCol["RESULTS_TABLE_TITLE"]?>
                            </th><?
                        } //endif(($arrCol["ADDITIONAL"]=="Y" && $SHOW_ADDITIONAL=="Y") || $arrCol["ADDITIONAL"]!="Y");
                    } //endif(!is_array($arrNOT_SHOW_TABLE) || !in_array($arrCol["SID"],$arrNOT_SHOW_TABLE));
                } //foreach
            } //endif(is_array($arrColumns)) ;
            ?>
        </tr>
        </thead>
        <?
        /***********************************************
        table body
         ************************************************/
        ?>
        <?
        if(count($arResult["arrResults"]) > 0)
        {
            ?>
            <tbody>
            <?
            $j=0;
            foreach ($arResult["arrResults"] as $arRes)
            {
                $j++;
?>


                <tr>
                    <td><?=$arRes["TSX_0"]?><br /><?=$arRes["TSX_1"]?></td>
                    <?
                    if ($arParams["F_RIGHT"] >= 25)
                    {
                        ?>
                        <td><?
                            if ($arRes["USER_ID"]>0)
                            {
                                $userName = array("NAME" => $arRes["USER_FIRST_NAME"], "LAST_NAME" => $arRes["USER_LAST_NAME"], "SECOND_NAME" => $arRes["USER_SECOND_NAME"], "LOGIN" => $arRes["LOGIN"]);
                                ?>
                                [<a title="<?=GetMessage("FORM_EDIT_USER")?>" href="/bitrix/admin/user_edit.php?lang=<?=LANGUAGE_ID?>&ID=<?=$arRes["USER_ID"]?>"><?=$arRes["USER_ID"]?></a>] (<?=$arRes["LOGIN"]?>) <?=CUser::FormatName($arParams["NAME_TEMPLATE"], $userName)?>
                                <?if($arRes["USER_AUTH"]=="N") { ?><?=GetMessage("FORM_NOT_AUTH")?><?}?>
                                <?
                            }
                            else
                            {
                                ?>
                                <?=GetMessage("FORM_NOT_REGISTERED")?>
                                <?
                            } // endif ($GLOBALS["f_USER_ID"]>0);
                            ?>
                            <?
                            if ($arParams["isStatisticIncluded"])
                            {
                                if (intval($arRes["STAT_GUEST_ID"])>0)
                                {
                                    ?>
                                    [<a title="<?=GetMessage("FORM_GUEST")?>" href="/bitrix/admin/guest_list.php?lang=<?=LANGUAGE_ID?>&find_id=<?=$arRes["STAT_GUEST_ID"]?>&set_filter=Y"><?=$arRes["STAT_GUEST_ID"]?></a>]
                                    <?
                                } //endif ((intval($GLOBALS["f_STAT_GUEST_ID"])>0));
                                ?>
                                <?
                                if (intval($arRes["STAT_SESSION_ID"])>0)
                                {
                                    ?>
                                    (<a title="<?=GetMessage("FORM_SESSION")?>" href="/bitrix/admin/session_list.php?lang=<?=LANGUAGE_ID?>&find_id=<?=$arRes["STAT_SESSION_ID"]?>&set_filter=Y"><?=$arRes["STAT_SESSION_ID"]?></a>)
                                    <?
                                } //endif ((intval($GLOBALS["f_STAT_SESSION_ID"])>0));
                            } //endif (isStatisitcIncluded);
                            ?></td>
                        <?
                    } //endif ($F_RIGHT>=25);
                    ?>
                    <?
                    foreach ($arResult["arrColumns"] as $FIELD_ID => $arrC)
                    {
                        if (!is_array($arParams["arrNOT_SHOW_TABLE"]) || !in_array($arrC["SID"], $arParams["arrNOT_SHOW_TABLE"]))
                        {
                            if (($arrC["ADDITIONAL"]=="Y" && $arParams["SHOW_ADDITIONAL"]=="Y") || $arrC["ADDITIONAL"]!="Y")
                            {
                                ?>
                                <td>
                                    <?
                                    $arrAnswer = $arResult["arrAnswers"][$arRes["ID"]][$FIELD_ID];
                                    if (is_array($arrAnswer))
                                    {
                                        foreach ($arrAnswer as $key => $arrA)
                                        {
                                            ?>
                                            <?if (strlen(trim($arrA["USER_TEXT"])) > 0) {?><?=$arrA["USER_TEXT"]?><br /><?}?>
                                            <?if (strlen(trim($arrA["ANSWER_TEXT"])) > 0) {?>[<span class='form-anstext'><?=$arrA["ANSWER_TEXT"]?></span>]&nbsp;<?}?>
                                            <?if (strlen(trim($arrA["ANSWER_VALUE"])) > 0 && $arParams["SHOW_ANSWER_VALUE"]=="Y") {?>(<span class='form-ansvalue'><?=$arrA["ANSWER_VALUE"]?></span>)<?}?>
                                            <br />
                                            <?
                                            if (intval($arrA["USER_FILE_ID"])>0)
                                            {
                                                if ($arrA["USER_FILE_IS_IMAGE"]=="Y")
                                                {
                                                    ?>
                                                    <?=$arrA["USER_FILE_IMAGE_CODE"]?>
                                                    <?
                                                }
                                                else
                                                {
                                                    ?>
                                                    <a title="<?=GetMessage("FORM_VIEW_FILE")?>" target="_blank" href="/bitrix/tools/form_show_file.php?rid=<?=$arRes["ID"]?>&hash=<?=$arrA["USER_FILE_HASH"]?>&lang=<?=LANGUAGE_ID?>"><?=$arrA["USER_FILE_NAME"]?></a><br />
                                                    (<?=$arrA["USER_FILE_SIZE_TEXT"]?>)<br />
                                                    [&nbsp;<a title="<?=str_replace("#FILE_NAME#", $arrA["USER_FILE_NAME"], GetMessage("FORM_DOWNLOAD_FILE"))?>" href="/bitrix/tools/form_show_file.php?rid=<?=$arRes["ID"]?>&hash=<?=$arrA["USER_FILE_HASH"]?>&lang=<?=LANGUAGE_ID?>&action=download"><?=GetMessage("FORM_DOWNLOAD")?></a>&nbsp;]
                                                    <?
                                                }
                                            }
                                            ?>
                                            <?
                                        } //foreach
                                    } // endif (is_array($arrAnswer));
                                    ?>
                                </td>
                                <?
                            } //endif (($arrC["ADDITIONAL"]=="Y" && $SHOW_ADDITIONAL=="Y") || $arrC["ADDITIONAL"]!="Y") ;
                        } // endif (!is_array($arrNOT_SHOW_TABLE) || !in_array($arrC["SID"],$arrNOT_SHOW_TABLE));
                    } //foreach
                    ?>
                </tr>
                <?
            } //foreach
            ?>
            </tbody>
            <?
        }
        ?>
        <?
        if ($arParams["HIDE_TOTAL"]!="Y")
        {
            ?>
            <tfoot>
            <tr>
                <th colspan="<?=$colspan?>"><?=GetMessage("FORM_TOTAL")?>&nbsp;<?=$arResult["res_counter"]?></th>
            </tr>
            </tfoot>
            <?
        } //endif ($HIDE_TOTAL!="Y");
        ?>
    </table>

    <p><?=$arResult["pager"]?></p>
    <?
    if (intval($arResult["res_counter"])>0 && $arParams["SHOW_STATUS"]=="Y" && $arParams["F_RIGHT"] >= 15)
    {
        ?>
        <p>
            <input type="submit" name="save" value="<?=GetMessage("FORM_SAVE")?>" /><input type="hidden" name="save" value="Y" />&nbsp;<input type="reset" value="<?=GetMessage("FORM_RESET")?>" />
        </p>
        <?
    } //endif (intval($res_counter)>0 && $SHOW_STATUS=="Y" && $F_RIGHT>=15);
    ?>

</form>