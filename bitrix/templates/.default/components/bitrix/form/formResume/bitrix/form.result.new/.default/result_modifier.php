<?
$arTempID = array();

foreach($arResult["ITEMS"] as $item){

    //dump(substr($item["PROPERTY_LINK_DETAIL_PAGE_URL"], 1));
    //$arTempID[] = substr($item["PROPERTY_LINK_DETAIL_PAGE_URL"], 1);
    $arTempID[] = $item["PROPERTIES"]["LINK"]["VALUE"];
}

//dump($arTempID);

$arSort = false;
$arFilter = array(
    "IBLOCK_ID" => IBLOCK_CAT_ID,
    "ACTIVE" => "Y",
    "ID" => $arParams,
);
$arGroupBy = false;
$arNavStartParams = array("nTopCount" => 50);
$arSelect = array("ID", "NAME", "DETAIL_PAGE_URL", "PROPERTY_PRICE", "PROPERTY_SIZE", "PROPERTY_PRICECURRENCY");
$BDRes = CIBlockElement::GetList(
    $arSort,
    $arFilter,
    $arGroupBy,
    $arNavStartParams,
    $arSelect
);
$arResult["CAT_ELEM"] = array();

while($arRes = $BDRes->GetNext()){

    //dump($arRes);

    $arResult["CAT_ELEM"][$arRes["ID"]] = $arRes;
}

//dump($arResult["CAT_ELEM"]);