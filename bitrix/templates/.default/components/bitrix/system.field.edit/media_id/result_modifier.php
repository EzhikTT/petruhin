<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/**
 * @var array $arResult
 */

if (!CModule::IncludeModule("iblock")) {
    return;
}

CModule::IncludeModule("fileman");
CMedialib::Init();

$arValue = array();

$dbRes = CMedialibCollection::GetList(array('arOrder' => Array('NAME' => 'ASC'), 'arFilter' => array('ACTIVE' => 'Y'))); //CIBlockSection::GetList(array('left_margin' => 'asc'), array('ID' => $arResult['VALUE']), false, array("ID", "NAME"));

$arResult['CONTENT_LIST'] = array();
$arResult['CONTENT_LIST'][0] = "���";

foreach ($dbRes as $arRes) {
    $arResult['CONTENT_LIST'][$arRes['ID']] = $arRes['NAME'];
}