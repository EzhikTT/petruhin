<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<script type="text/javascript" >
	$(document).ready(function(){

		$("#foo").carouFredSel({
			items: {
				visible: 3,
				width: 300,
				height: 300,
			},
			/*prev:'#rwprev',
			next:'#rwnext',*/
			/*scroll:{
				items:1,
				duration:2000
			},*/
			width: null,
			/*data: {
				width: 1500,
			},*/
			//height: 300,
			//pagination: true,
			/*width: 'auto',*/
			//items: 4,
			scroll: 1,
			/*auto: {
				//duration: 1250,
				timeoutDuration: 400,
			},*/
			prev: '#rwprev',
			next: '#rwnext',
			//pagination: false
		});
	});
</script>
<div class="rw_reviewed">
	<div class="rw_slider">
		<h4>������</h4>
		<ul id="foo">
			<?foreach($arResult["ITEMS"] as $arItem):?>
			<li>
				<div class="rw_message">
					<img src="/bitrix/templates/.default/content/photo_1.jpg" class="rw_avatar" alt=""/>
					<span class="rw_name"><?echo $arItem["NAME"]?></span>
					<span class="rw_job"><?echo $arItem["PROPERTIES"]["POST"]["VALUE"]?> <?echo $arItem["PROPERTIES"]["NAME_ORGANIZATION"]["VALUE"]?></span>
					<p>�<?echo $arItem["PREVIEW_TEXT"];?>�</p>
					<div class="clearboth"></div>
					<div class="rw_arrow"></div>
				</div>
			</li>
			<?endforeach;?>
		</ul>
		<div id="rwprev"></div>
		<div id="rwnext"></div>
		<a href="/company/otzyvy/" class="rw_allreviewed">��� ������</a>
	</div>
</div>