<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true){
	die();
}
?>
<?
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<?$APPLICATION->ShowHead();?>
	<title><?$APPLICATION->ShowTitle()?></title>
	<!--<link rel="stylesheet" href="/bitrix/templates/.default/template_styles.css" type="text/css" />-->
	<link rel="stylesheet" href="/bitrix/templates/.default/template_styles.css"/>
	<script type="text/javascript" src="/bitrix/templates/.default/js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="/bitrix/templates/.default/js/slides.min.jquery.js"></script>
	<script type="text/javascript" src="/bitrix/templates/.default/js/jquery.carouFredSel-6.1.0-packed.js"></script>
	<script type="text/javascript" src="/bitrix/templates/.default/js/functions.js"></script>
	<?
	/*$APPLICATION->SetAdditionalCSS("/bitrix/templates/.default/template_styles.css");
	$APPLICATION->AddHeadScript("/bitrix/templates/.default/js/jquery-1.8.2.min.js");
	$APPLICATION->AddHeadScript("/bitrix/templates/.default/js/slides.min.jquery.js");
	$APPLICATION->AddHeadScript("/bitrix/templates/.default/js/jquery.carouFredSel-6.1.0-packed.js");
	$APPLICATION->AddHeadScript("/bitrix/templates/.default/js/functions.js");*/
	?>
	<link rel="shortcut icon" type="image/x-icon" href="/bitrix/templates/.default/favicon.ico"/>
	
	<!--[if gte IE 9]><style type="text/css">.gradient {filter: none;}</style><![endif]-->
</head>
<body>
<?$APPLICATION->ShowPanel();?>
	<div class="wrap">
		<div class="hd_header_area">
			<?include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/.default/include/header.php");?>
		</div>
		
		<!--- // end header area --->

		<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"slider_main", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "slider_main",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "PROPERTY_LINK.NAME",
			2 => "PROPERTY_LINK.CODE",
			3 => "PROPERTY_LINK.PROPERTY_PRICE",
			4 => "PROPERTY_LINK.PROPERTY_PRICECURRENCY",
			5 => "PROPERTY_LINK.DETAIL_PAGE_URL",
			6 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "7",
		"IBLOCK_TYPE" => "shares",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "10",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "�������",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "COST",
			1 => "LINK",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	),
	false
);?><br>

		
		<!--- // end slider area --->
		
		<div class="main_container homepage">
			
			<!-- events -->
			<div class="ev_events">
				<div class="ev_h">
					<h3>��������� �������</h3>
					<a href="" class="ev_allevents">��� ����������� &rarr;</a>
				</div>
				<ul class="ev_lastevent">
					<li>
						<h4><a href="">29 ������� 2012, ������</a></h4>
						<p>������� �������������� ������ ������ � ���, ���������� ���������.</p>
					</li>
					<li>
						<h4><a href="">30 ������� 2012, �����-���������</a></h4>
						<p>�������� ���-���� �� ������� ���������. ��������� ������ � ������� ������������.</p>
					</li>
					<li>
						<h4><a href="">31 ������� 2012, ���������</a></h4>
						<p>�������� ������ �������� � ����� ��������� ����.</p>
					</li>
				</ul>
				<div class="clearboth"></div>
			</div>
			<!-- // end events -->
			<div class="cn_hp_content">
				<div class="cn_hp_category">
					<ul>
						<li>
							<img src="/bitrix/templates/.default/content/1.png" alt=""/>
							<h2><a href="">������ ������</a></h2>
							<p>������, ������ � ������ ������ ������ <a class="cn_hp_categorymore" href="">&rarr;</a></p>
							<div class="clearboth"></div>
						</li>
						<li>
							<img src="content/2.png" alt=""/>
							<h2><a href="">������� ������</a></h2>
							<p>������, �����, ������ <a class="cn_hp_categorymore" href="">&rarr;</a></p>
							<div class="clearboth"></div>
						</li>
						<li>
							<img src="/bitrix/templates/.default/content/3.png" alt=""/>
							<h2><a href="">������ ��� �����</a></h2>
							<p>�����, �����, ����� � ������ <a class="cn_hp_categorymore" href="">&rarr;</a></p>
							<div class="clearboth"></div>
						</li>
						<li>
							<img src="/bitrix/templates/.default/content/4.png" alt=""/>
							<h2><a href="">������� ������</a></h2>
							<p>�������, ������, ������ ������� ������ <a class="cn_hp_categorymore" href="">&rarr;</a></p>
							<div class="clearboth"></div>
						</li>
					</ul>
					<a href="" class="cn_hp_category_more">��� ������� �������� &rarr;</a>
				</div>
				<div class="cn_hp_post">
					<div class="cn_hp_post_new">
						<h3>�������</h3>
						<img src="/bitrix/templates/.default/content/7.png" alt=""/>
						<p>������� ����� "�������", � ������� ������� ��������� � ������.</p>
						<div class="clearboth"></div>
					</div>
					<div class="cn_hp_post_action">
						<h3>�����</h3>
						<img src="/bitrix/templates/.default/content/7.png" alt=""/>
						<p>������� ����� "�������", � ������� ������� ��������� � ������.</p>
						<div class="clearboth"></div>
					</div>
					<div class="cn_hp_post_bestsellersn">
						<h3>���� ������</h3>
						<img src="/bitrix/templates/.default/content/7.png" alt=""/>
						<p>������� ����� "�������", � ������� ������� ��������� � ������.</p>
						<div class="clearboth"></div>
					</div>
				</div>
				<div class="cn_hp_lastnews">
					<h3><a href="">�������</a></h3>		
					<ul>
						<li>
							<h4><a href="">29 ������� 2012</a></h4>
							<p>����������� ������ ������� ������ �� ��������</p>
						</li>
						<li>
							<h4><a href="">29 ������� 2012</a></h4>
							<p>������-����� ���������� �� ������ ��� �������������� ������</p>
						</li>
						<li>
							<h4><a href="">29 ������� 2012</a></h4>
							<p>�������� ������ ������ ����� ����� � ����� ���������</p>
						</li>
						<li>
							<h4><a href="">29 ������� 2012</a></h4>
							<p>���� ��������� ���� ����������� ������ ����������� ����� ������� �������� � ������</p>
						</li>					
					</ul>
					<br/>
					<a href="" class="cn_hp_lastnews_more">��� ������� &rarr;</a>
				</div>
				<div class="clearboth"></div>
			</div>
		</div>

		<?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"reviews", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "reviews",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "reviews",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "4",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "�������",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "POST",
			1 => "NAME_ORGANIZATION",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	),
	false
);?><br>